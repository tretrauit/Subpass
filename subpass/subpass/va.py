import pulsectl
import random
try:
    from subpass.utils import Utilities
except ImportError:
    from .utils import Utilities


class VirtualAudio:
    def __init__(self):
        self.pulse = pulsectl.Pulse("Subpass")  # Create a pulse client

    def get_virtual_sink_list(self, usable=True, unusable=True):
        sink_list = self.pulse.sink_list()
        for v in sink_list:
            if (
                ("Subpass.Speaker" not in v.name)
                or ("Subpass.Speaker.True" in v.name and not usable)
                or ("Subpass.Speaker.False" in v.name and not unusable)
            ):
                sink_list.remove(v)
        return sink_list

    def get_virtual_sink(self, name):
        for sink in self.get_virtual_sink_list():
            if name in sink.name:
                return sink
        return

    def get_virtual_source_list(self, usable=True, unusable=True):
        source_list = self.pulse.source_list()
        for v in source_list:
            if (
                ("Subpass.Microphone" not in v.name)
                or ("Subpass.Microphone.True" in v.name and not usable)
                or ("Subpass.Microphone.False" in v.name and not unusable)
            ):
                source_list.remove(v)
        return source_list

    def get_virtual_source(self, name):
        for sink in self.get_virtual_source_list():
            if name in sink.name:
                return sink
        return

    @staticmethod
    def open_pavucontrol():
        Utilities.check_dependencies("pavucontrol")
        Utilities.popen(["pavucontrol"])

    def create_virtual_sink(self, sink_name="Virtual-Device", usable=True):
        sink_type = "module-null-sink"
        sink_name = f"Subpass.Speaker.{usable}.{''.join(sink_name.split())}"
        if self.get_virtual_sink(sink_name):
            raise ValueError(f"Virtual sink with name '{sink_name}' already exists.")
        if usable:
            sink_type = "module-remap-sink"
        print("Creating virtual sink: " + sink_type)
        Utilities.run(["pactl", "load-module", sink_type, f"sink_name='{sink_name}'"])

    def create_virtual_source(self, source_name="Virtual-Device", usable=True):
        print(
            "Virtual source is not stable yet, please use Monitor source of Virtual sink if you can."
        )
        source_type = "module-remap-source"
        if self.get_virtual_sink(source_name):
            raise ValueError(
                f"Virtual source with name '{source_name}' already exists."
            )
        if not usable:
            raise NotImplementedError(
                "Unusable source is not implemented yet, please use usable source and mute it in pavucontrol."
            )
            # null_sink = self.get_virtual_sink("Subpass.Speaker.False")
            # if not null_sink:
            #     self.create_virtual_sink(source_name, usable=False)
            # null_sink = self.get_virtual_sink(source_name)
        print("Creating virtual source: " + source_type)
        Utilities.run(
            [
                "pactl",
                "load-module",
                source_type,
                f"source_name='Subpass.Microphone.{usable}.{''.join(source_name.split())}'",
            ]
        )

    def create_device(
        self, device_type, device_name="Virtual-Device", usable=True, callbacks=None
    ):
        print("callbacks is not implemented yet (VirtualAudio.create_device)")
        device_type = device_type.lower()
        device = None
        if device_type == "speaker":
            device = self.create_virtual_sink(device_name, usable)
        elif device_type == "microphone":
            device = self.create_virtual_source(device_name, usable)
        else:
            raise ValueError(f"Unknown device type '{device_type}'")
        return device  # dummy
