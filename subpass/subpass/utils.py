import shutil
import subprocess
import os


class Utilities:
    def __init__(self):
        pass

    @staticmethod
    def run(args=None, env=os.environ, cwd=None, shell=False, stdout=None, stderr=None):
        result = subprocess.run(
            args, env=env, cwd=cwd, shell=shell, stdout=stdout, stderr=stderr
        )
        result.check_returncode()
        return result

    @staticmethod
    def popen(
        args=None,
        env=os.environ,
        cwd=None,
        shell=False,
        stdin=None,
        stdout=None,
        stderr=None,
    ):
        return subprocess.Popen(
            args,
            env=env,
            cwd=cwd,
            shell=shell,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
        )

    @staticmethod
    def check_dependencies(dependencies):
        if not isinstance(dependencies, list):
            dependencies = [dependencies]
        for dependency in dependencies:
            if not shutil.which(dependency):
                raise OSError("Dependency not found: {}".format(dependency))
        return True

    @staticmethod
    def get_env_dict(env_str, sep="\n"):
        """
        Returns a dictionary of environment variables from a string.
        :param env_str: string containing environment variables
        :param sep: seperator for the string
        :return: A dictionary containing the formatted environment variables.
        """
        env = {}
        for line in env_str.split(sep):
            # Split the line into key and value
            env_var = line.split("=")
            env[env_var[0]] = "=".join(env_var[1:])
        return env
