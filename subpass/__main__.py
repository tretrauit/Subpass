try:
    from widget import main
except ImportError:
    from .widget import main

if __name__ == "__main__":
    main()
