import random
import os
import threading
import time
from subprocess import Popen, check_output, PIPE, STDOUT
from pathlib import Path
try:
    from subpass.utils import Utilities
except ImportError:
    from .utils import Utilities

def _initialize_dbus():
    dbus_session = check_output("dbus-launch")
    dbus_env = Utilities.get_env_dict(dbus_session.decode("utf-8"))
    return dbus_env

# Virtual desktop related functions
class VirtualDesktop:
    def __init__(self):
        self.x11_dir = Path("/tmp/.X11-unix")
        self.xdg_runtime_dir = Path(os.environ["XDG_RUNTIME_DIR"])
        self.desktops = []

    def _get_nonexistent_xdisplay(self):
        x_display = 0
        while self.x11_dir.joinpath("X{}".format(x_display)).exists():
            x_display = random.randint(0, 2**31 - 1)
        return x_display

    def _get_wayland_displays(self, display_name="wayland"):
        displays = []
        for path in self.xdg_runtime_dir.iterdir():
            if display_name in path.name and not path.suffix == ".lock":
                displays.append(path.name)
        return displays

    def _create_desktop_xephyr(self, width, height, glamor, resizable, ibus, fcitx5):
        vd_env = os.environ | _initialize_dbus()
        x_display = self._get_nonexistent_xdisplay()
        ibus_daemon = None
        fcitx5_daemon = None
        args = [
            "Xephyr",
            "-br",
            "-ac",
            "-noreset",
            "-screen",
            f"{width}x{height}",
            ":{}".format(x_display),
        ]
        if glamor:
            print("glamor enabled, Xephyr will have software rendering.")
            args.append("-glamor")
        if resizable:
            print("resizable enabled, Xephyr will have resizable window.")
            args.append("-resizeable")

        print("Starting Xephyr...")
        xephyr = Popen(args, env=vd_env, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        vd_env |= {"DISPLAY": ":{}".format(x_display)}

        if ibus:
            print("Starting IBus daemon for Xephyr...")
            ibus_daemon = Popen(["ibus-daemon"], env=vd_env)
        if fcitx5:
            print("Starting IBus daemon for KWin...")
            fcitx5_daemon = Popen(["fcitx5"], env=vd_env)
        return {
            "env": vd_env,
            "desktop": xephyr,
            "x_display": x_display,
            "wayland_display": None,  # Xephyr is X11 only.
            "ibus": ibus_daemon,
            "fcitx5": fcitx5_daemon,
        }

    def _create_desktop_kwin(
        self, width, height, xwayland, ibus, fcitx5, kde_integration
    ):
        vd_env = os.environ | _initialize_dbus()
        x_display = None
        wayland_display = "subpass-" + str(random.randint(0, 2**31 - 1))
        print("Generated Wayland socket: {}".format(wayland_display))
        ibus_daemon = None
        fcitx5_daemon = None
        args = [
            "kwin_wayland",
            "--socket",
            wayland_display,
            "--width",
            str(width),
            "--height",
            str(height),
        ]
        if not kde_integration:
            args.extend(
                [
                    "--no-lockscreen",
                    "--no-global-shortcuts",
                    "--no-kactivities",
                ]
            )
        if xwayland:
            print("xwayland is enabled, KWin will have X11 support.")
            args.append("--xwayland")

        prev_x_displays = [v.name for v in self.x11_dir.iterdir()]
        kwin = Popen(args, env=vd_env, stdout=PIPE, stderr=STDOUT)
        vd_env |= {"WAYLAND_DISPLAY": wayland_display}
        # Abuse stdout to do loops because when it stops printing to stdout we must already have the values
        for _ in kwin.stdout:
            if (x_display and xwayland) or not xwayland:
                break
            if xwayland:
                print("Trying to find X display ID because XWayland is enabled...")
                cur_x_displays = [v.name for v in Path(self.x11_dir).iterdir()]
                print(cur_x_displays)
                new_x_display = list(set(cur_x_displays) - set(prev_x_displays))
                print(new_x_display)
                if new_x_display:
                    x_display = new_x_display[0][1:]
                    print("Found X window ID: {}".format(x_display))
                    vd_env |= {"DISPLAY": ":{}".format(x_display)}
            time.sleep(0.05)

        if ibus:
            print("Starting IBus daemon for KWin...")
            ibus_daemon = Popen(["ibus-daemon"], env=vd_env)

        if fcitx5:
            print("Starting IBus daemon for KWin...")
            fcitx5_daemon = Popen(["fcitx5"], env=vd_env)

        return {
            "env": vd_env,
            "desktop": kwin,
            "x_display": x_display,
            "wayland_display": wayland_display,
            "ibus": ibus_daemon,
            "fcitx5": fcitx5_daemon,
        }

    def _create_desktop_gamescope(self, width, height, ibus, fcitx5):
        vd_env = os.environ | _initialize_dbus()
        x_display = None
        wayland_display = None
        ibus_daemon = None
        fcitx5_daemon = None
        args = [
            "gamescope",
            "-w",
            str(width),
            "-W",
            str(width),
            "-h",
            str(height),
            "-H",
            str(height),
        ]

        prev_x_displays = [v.name for v in self.x11_dir.iterdir()]
        prev_wayland_displays = self._get_wayland_displays()
        kwin = Popen(args, env=vd_env, stdout=PIPE, stderr=STDOUT)
        print("Trying to find Wayland display Gamescope is using...")
        # Abuse stdout to do loops because when it stops printing to stdout we must already have the values
        for _ in kwin.stdout:
            if wayland_display is None:
                wayland_displays = self._get_wayland_displays("gamescope")
                new_wayland_display = list(
                    set(wayland_displays) - set(prev_wayland_displays)
                )
                if new_wayland_display:
                    wayland_display = new_wayland_display[0]
                    print("Found Wayland display {}".format(wayland_display))
                    vd_env |= {"WAYLAND_DISPLAY": wayland_display}
            if x_display is None:
                print("Trying to find X display ID...")
                cur_x_displays = [v.name for v in self.x11_dir.iterdir()]
                new_x_display = list(set(cur_x_displays) - set(prev_x_displays))
                if new_x_display:
                    x_display = new_x_display[0][1:]
                    print("Found X window ID: {}".format(x_display))
                    vd_env |= {"DISPLAY": ":{}".format(x_display)}
            if wayland_display and x_display:
                break

        if ibus:
            print("Starting IBus daemon for Gamescope...")
            ibus_daemon = Popen(["ibus-daemon"], env=vd_env)

        if fcitx5:
            print("Starting IBus daemon for KWin...")
            fcitx5_daemon = Popen(["fcitx5"], env=vd_env)

        return {
            "env": vd_env,
            "desktop": kwin,
            "x_display": x_display,
            "wayland_display": wayland_display,
            "ibus": ibus_daemon,
            "fcitx5": fcitx5_daemon,
        }

    def get_desktop(self, name):
        for desktop in self.desktops:
            if desktop["name"] == name:
                return desktop
        return None

    def run_application(self, args, desktop_name, env):
        desktop = self.get_desktop(desktop_name)
        if desktop is None:
            raise ValueError(
                "Desktop with name '{}' does not exist.".format(desktop_name)
            )
        if args is None or args == "":
            raise ValueError("Invalid arguments for application.")
        if isinstance(args, str):
            args = args.split(" ")  # Will use shlex for better quote handling later.

        if env is None:
            env = {}
        if isinstance(env, str):
            env = Utilities.get_env_dict(env)
        env = desktop["env"] | env

        print("Env: {}".format(env))
        print("Running application {} on desktop '{}'...".format(args, desktop_name))
        desktop["applications"].append(Popen(args, env=env))
        print("Appended application to list of applications.")

    def create_desktop(
        self,
        de_type="KWin",
        width=1280,
        height=720,
        name="amogus sussy",
        xwayland=True,
        ibus=False,
        fcitx5=False,
        no_kde_integration=True,
        glamor=True,
        resizable=True,
        callbacks=None,
    ):
        print(de_type, width, height, name, xwayland, ibus, fcitx5, no_kde_integration, glamor, resizable, callbacks)
        if self.get_desktop(name) is not None:
            raise ValueError("Desktop with name '{}' already exists.".format(name))
        if name is None or name.strip() == "":
            raise ValueError("Invalid name '{}' for desktop.".format(name))
        Utilities.check_dependencies("dbus-launch")
        if ibus:
            Utilities.check_dependencies("ibus-daemon")
        if fcitx5:
            Utilities.check_dependencies("fcitx5")
        desktop = None
        if de_type == "KWin":
            Utilities.check_dependencies(["kwin_wayland"])
            desktop = self._create_desktop_kwin(
                width=width,
                height=height,
                xwayland=xwayland,
                ibus=ibus,
                fcitx5=fcitx5,
                kde_integration=not no_kde_integration,
            )
        elif de_type == "gamescope":
            Utilities.check_dependencies("gamescope")
            desktop = self._create_desktop_gamescope(
                width=width, height=height, ibus=ibus, fcitx5=fcitx5
            )
        elif de_type == "Xephyr":
            Utilities.check_dependencies("Xephyr")
            desktop = self._create_desktop_xephyr(
                width=width,
                height=height,
                glamor=glamor,
                resizable=resizable,
                ibus=ibus,
                fcitx5=fcitx5,
            )
        else:
            raise ValueError("Unknown desktop type: {}".format(de_type))
        desktop |= {"name": name, "type": de_type, "applications": []}
        print("Created desktop with name: {}".format(name))
        self.desktops.append(desktop)
        print("Appended desktop to list of desktops.")
        print("Preparing functions for closing...")

        def default_callback():
            nonlocal desktop
            for app in desktop["applications"]:
                if app.poll() is None:
                    print("Exiting {}...".format(app.pid))
                    app.terminate()
            if desktop["ibus"] is not None:
                print("Exiting IBus...")
                desktop["ibus"].terminate()
            if desktop["fcitx5"] is not None:
                print("Exiting Fcitx5...")
                desktop["fcitx5"].terminate()
            for vd in self.desktops:
                if vd["name"] == desktop["name"]:
                    self.desktops.remove(vd)
                    break

        callbacks.append({"callback": default_callback, "args": []})

        # Function wait for desktop to exit
        def wait_for_exit(*cbs):
            print(cbs)
            nonlocal desktop
            desktop["desktop"].wait()
            for callback in cbs:
                print(callback)
                callback["callback"](*callback["args"])

        thread = threading.Thread(target=wait_for_exit, args=callbacks)
        thread.start()
        return desktop
