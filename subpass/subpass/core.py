try:
    from subpass.vd import VirtualDesktop
    from subpass.va import VirtualAudio
except ImportError:
    from .vd import VirtualDesktop
    from .va import VirtualAudio


class Subpass:
    def __init__(self):
        self.vd = VirtualDesktop()
        self.va = VirtualAudio()
        # Aliases
        self.VirtualDesktop = self.vd
        self.VirtualAudio = self.va
