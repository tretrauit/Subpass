from pathlib import Path
from shutil import which, rmtree, copy2
import argparse
import subprocess
import importlib.util
import sys


def run(args, cwd=Path.cwd(), shell=False):
    print("Running:", args)
    result = subprocess.run(args, cwd=cwd, shell=shell)
    result.check_returncode()


def run_nuitka(args, cwd):
    if which("nuitka3"):
        print("Using system nuitka3...")
        run(["nuitka3", *args], cwd=cwd)
        return True
    print("nuitka3 not found, falling back to python module.")
    nuitka = importlib.util.find_spec("nuitka")
    if nuitka is None:
        print("nuitka module not found, installing...")
        run([sys.executable, "-m", "pip", "install", "nuitka"])
    run([sys.executable, "-m", "nuitka", *args], cwd=cwd)


def main():
    parser = argparse.ArgumentParser(description="Subpass build script.")
    parser.add_argument(
        "--standalone",
        action="store_true",
        help="Build standalone binary with Qt 6 and PySide6 included.",
    )
    parser.add_argument(
        "--pack", action="store_true", help="Compress files into a .tar.zst archive."
    )
    cwd = Path.cwd()
    if cwd.name == "scripts":
        cwd = cwd.parent
    build_path = cwd.joinpath("build")
    print("Removing old build directory...")
    rmtree(build_path, ignore_errors=True)
    build_path.mkdir(exist_ok=True)
    source_path = cwd.joinpath("src")
    widget_path = source_path.joinpath("widget.py").resolve()
    args = parser.parse_args()
    if args.standalone:
        print("Building standalone binary...")
        run_nuitka(
            ["--standalone", "--enable-plugin=pyside6", widget_path], cwd=build_path
        )
    else:
        print("Building dynamic binary...")
        run_nuitka(
            ["--enable-plugin=pyside6", "--follow-imports", widget_path], cwd=build_path
        )
    print("Copying files...")
    if args.standalone:
        copy2(
            source_path.joinpath("form.ui"), build_path.joinpath("widget.dist/form.ui")
        )
    else:
        copy2(source_path.joinpath("form.ui"), build_path.joinpath("form.ui"))
    print("Build completed.")
    if not args.pack:
        return
    if not which("tar") or not which("zstd"):
        print("tar or zstd not found, can't pack.")
        return
    print("Packing files...")
    if args.standalone:
        run(
            'tar "-I zstd -19 -T4" -cf Subpass.tar.zst widget.dist/',
            cwd=build_path,
            shell=True,
        )
    else:
        run(
            'tar "-I zstd -19 -T4" -cf Subpass_SysDeps.tar.zst widget.bin form.ui',
            cwd=build_path,
            shell=True,
        )
    print("Packing completed.")


if __name__ == "__main__":
    main()
