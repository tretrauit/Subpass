import random
from PySide6.QtCore import Slot
from PySide6.QtWidgets import QMessageBox
try:
    from subpass.core import Subpass
except ImportError:
    from .subpass.core import Subpass

class VD:
    def __init__(self, subpass_core, window):
        self.subpass_core = subpass_core
        self.window = window
        self.vd = subpass_core.vd

    @Slot()
    def create_desktop(self):
        # Optimized code ya
        print("Create desktop function clicked on UI.")
        nameTb = self.window.vdNameTb

        de_type = self.window.vdModeList.currentText()
        width = self.window.vdWidthSb.value()
        height = self.window.vdHeightSb.value()
        name = nameTb.toPlainText()
        xwayland = self.window.vdKwinXwaylandCb.isChecked()
        ibus = self.window.vdIbusCb.isChecked()
        fcitx5 = self.window.vdFcitx5Cb.isChecked()
        kde_integration = self.window.vdKdeIntegCb.isChecked()
        glamor = self.window.vdXephyrGlamorCb.isChecked()
        resizable = self.window.vdXephyrResizableCb.isChecked()

        VDlist = self.window.vdVDList
        if de_type == "KWin (Wayland)":
            de_type = "KWin"
        elif de_type == "Xephyr (Xorg)":
            de_type = "Xephyr"
        elif de_type == "gamescope (Wayland)":
            de_type = "gamescope"

        if name.strip() == "":
            name = str(random.randint(1000000000000000, 9999999999999999))
            nameTb.setPlainText(name)

        def callback():
            current_idx = self.window.vdVDList.findText(name)
            print("Removing item from index: ", current_idx)
            VDlist.removeItem(current_idx)
            # PySide6 doesn't trigger for us :(
            self.update_desktop_status()

        callback_dict = {"callback": callback, "args": []}
        try:
            self.vd.create_desktop(
                de_type=de_type,
                width=width,
                height=height,
                name=name,
                xwayland=xwayland,
                ibus=ibus,
                fcitx5=fcitx5,
                no_kde_integration=kde_integration,
                glamor=glamor,
                resizable=resizable,
                callbacks=[callback_dict],
            )
        except (ValueError, OSError) as e:
            Window.create_msgbox(
                "An error has been occurred",
                info_text=str(e),
                icon=QMessageBox.Critical,
            )
            return
        self.window.vdVDList.addItem(name)
        # Select the new desktop, bloated code
        self.window.vdVDList.setCurrentIndex(self.window.vdVDList.findText(name))
        Window.create_msgbox(
            "Desktop '{}' with type '{}' has been started.".format(name, de_type)
        )

    @Slot()
    def update_desktop_status(self):
        vdList = self.window.vdVDList
        name = self.window.vdVDList.currentText()
        vdTypeTxt = self.window.vdVDTypeTxt
        vdXdisplayTxt = self.window.vdVDXdisplayTxt
        vdWldisplayTxt = self.window.vdVDWldisplayTxt
        print("Update status for {}".format(name))
        desktop = self.vd.get_desktop(name)
        print(desktop)
        if desktop is None:
            # TODO message box
            print("Desktop not found.")
            if vdList.count() == 0:
                print("No other desktop found.")
                vdTypeTxt.setText("Desktop type: None")
                vdXdisplayTxt.setText("X display: None")
                vdWldisplayTxt.setText("Wayland display: None")
                return

            print("Selecting last desktop.")
            vdList.setCurrentIndex(vdList.count() - 1)
            return

        vdTypeTxt.setText("Desktop type: {}".format(desktop["type"]))
        vdXdisplayTxt.setText("X display: {}".format(desktop["x_display"]))
        vdWldisplayTxt.setText("Wayland display: {}".format(desktop["wayland_display"]))

    @Slot()
    def stop_desktop(self):
        name = self.window.vdVDList.currentText()
        desktop = self.vd.get_desktop(name)
        if desktop is None:
            print("Desktop '{}' not found.".format(name))
            return
        desktop["desktop"].terminate()
        print("Stopped desktop {}".format(name))
        Window.create_msgbox("Desktop '{}' has been stopped.".format(name))

    @Slot()
    def run_application(self):
        app = self.window.vdAppTb.toPlainText()
        desktop = self.window.vdVDList.currentText()
        env = self.window.vdAppEnvTb.toPlainText()
        try:
            self.vd.run_application(app, desktop, env)
        except (ValueError, FileNotFoundError, OSError) as e:
            Window.create_msgbox(
                "An error has been occurred",
                info_text=str(e),
                icon=QMessageBox.Critical,
            )
            return

        Window.create_msgbox("Application '{}' has been started.".format(app))


class VA:
    def __init__(self, subpass_core, window):
        self.subpass_core = subpass_core
        self.window = window
        self.va = subpass_core.va

    def create_audio(self):
        # Optimized code ya
        print("Create desktop function clicked on UI.")
        nameTb = self.window.vaNameTb

        audio_type = self.window.vaModeList.currentText()
        name = nameTb.toPlainText()
        usable_device = self.window.vaUsableCb.isChecked()

        VAlist = self.window.vaVAList

        if name.strip() == "":
            name = str(random.randint(1000000000000000, 9999999999999999))
            nameTb.setPlainText(name)

        # This function never get triggered.
        def callback():
            current_idx = self.window.vaVAList.findText(name)
            print("Removing item from index: ", current_idx)
            VAlist.removeItem(current_idx)
            # PySide6 doesn't trigger for us :(
            # self.update_desktop_status()

        callback_dict = {"callback": callback, "args": []}
        try:
            self.va.create_device(audio_type, name, usable_device, [callback_dict])
        except (
            ValueError,
            OSError,
            NotImplementedError,
        ) as e:  # There are unimplemented functions.
            Window.create_msgbox(
                "An error has been occurred",
                info_text=str(e),
                icon=QMessageBox.Critical,
            )
            return
        VAlist.addItem(name)
        # Select the new desktop, bloated code
        VAlist.setCurrentIndex(self.window.vdVDList.findText(name))
        Window.create_msgbox(
            "Audio device '{}' with type '{}' has been started.".format(
                name, audio_type
            )
        )

    def open_pavucontrol(self):
        print("Open pavuc function clicked on UI.")
        self.va.open_pavucontrol()


class Window:
    def __init__(self, window):
        self.subpass = Subpass()
        self.vd_ui = VD(subpass_core=self.subpass, window=window)
        self.window = window
        self.window.vdCreateBtn.clicked.connect(lambda: self.vd_ui.create_desktop())
        self.window.vdRunAppBtn.clicked.connect(lambda: self.vd_ui.run_application())
        self.window.vdVDList.currentIndexChanged.connect(
            lambda: self.vd_ui.update_desktop_status()
        )
        self.window.vdVDStopBtn.clicked.connect(lambda: self.vd_ui.stop_desktop())

        self.va_ui = VA(subpass_core=self.subpass, window=window)
        self.window.vaCreateBtn.clicked.connect(lambda: self.va_ui.create_audio())
        self.window.vaOpenPavucBtn.clicked.connect(
            lambda: self.va_ui.open_pavucontrol()
        )

    @staticmethod
    def create_msgbox(
        text,
        info_text=None,
        buttons=QMessageBox.Ok,
        default_button=QMessageBox.Ok,
        detailed_text=None,
        icon=QMessageBox.Information,
        execute=True,
    ):
        msg_box = QMessageBox()
        msg_box.setText(text)
        if info_text:
            msg_box.setInformativeText(info_text)
        custom_buttons = []
        if isinstance(buttons, (list, tuple, set)):
            for btn in buttons:
                if isinstance(btn[0], str):
                    custom_buttons.append(msg_box.addButton(btn[0], btn[1]))
                else:
                    custom_buttons.append(msg_box.addButton(btn[0]))
        else:
            msg_box.setStandardButtons(buttons)
        msg_box.setDefaultButton(default_button)
        if detailed_text:
            msg_box.setDetailedText(detailed_text)
        msg_box.setIcon(icon)
        if execute:
            if custom_buttons:
                return msg_box.exec(), custom_buttons
            return msg_box.exec()

        if custom_buttons:
            return msg_box, custom_buttons
        return msg_box

    def closeEvent(self, event):
        vdisplays = self.vd_ui.vd.desktops
        vdisplays_len = len(vdisplays)
        if vdisplays_len > 0:
            result = Window.create_msgbox(
                "There are currently {} virtual desktops running".format(vdisplays_len),
                info_text="Do you want to close them then exit?",
                buttons=QMessageBox.Yes | QMessageBox.No,
                default_button=QMessageBox.Yes,
                icon=QMessageBox.Question,
            )

            if result == QMessageBox.No:
                event.ignore()
                return

            for vdisplay in vdisplays:
                vdisplay["desktop"].terminate()

        event.accept()
