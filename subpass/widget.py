#!/usr/bin/python3
# This Python file uses the following encoding: utf-8
import os
from pathlib import Path
from PySide6.QtWidgets import QApplication, QWidget
from PySide6.QtCore import QFile
from PySide6.QtUiTools import QUiLoader

try:
    import subpassui
except ImportError:
    from . import subpassui


class Widget(QWidget):
    def __init__(self):
        super(Widget, self).__init__()
        self.subpassui = None
        self.load_ui()

    def load_ui(self):
        loader = QUiLoader()
        path = os.fspath(Path(__file__).resolve().parent / "form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self.subpassui = subpassui.Window(loader.load(ui_file, self))
        ui_file.close()

    def closeEvent(self, event):
        if self.subpassui is not None:
            print("Triggering closeEvent in subpassui...")
            self.subpassui.closeEvent(event=event)


def main():
    app = QApplication([])
    widget = Widget()
    widget.show()
    exit(app.exec())


if __name__ == "__main__":
    main()
